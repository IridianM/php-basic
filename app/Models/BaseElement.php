<?php
//Antes no eran namespaces, se identificaban de la siguiente manera:
    //class app_Models. para evitar conflictos con los archivos que tyuvieran el mismo nombre, librerias o proyectos.
    
namespace App\Models;
require_once 'Printable.php';



 //clases con php.
 class BaseElement implements Printable {
    //protected solo hijos tienen acceso al padre.
    public $title;
    public $description;
    public $visible = true;
    public $months;

    //Inicializando un constructor.
    public function __construct($title,  $description) {
        $this->title = $title;
        $this->description = $description;
    }

    //Métodos.
    public function setTitle ( $t ) {
        $this->title = $t;
    }
    public function getTitle () {
        return $this->title;
    }

    //retornar los meses que tenemos trabajando.
    public function getDurationAsString () {
        $years = floor( $this->months / 12 );
        $extraMonths = $this->months % 12;
    
        return  " $years years and  $extraMonths months. ";
    }
    // PRINTABLE OR INTERFACE.
    public function getDescription () {
        return $this->description;
    }

  }