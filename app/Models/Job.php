<?php
namespace App\Models;


class Job extends BaseElement {
    //Este constructor sobreescribira el que esta en la clase padre.
    public function __construct ($title, $description) {
        //podemos hacer appends nuevos aquí que solo esta clase va a tener.
        $newTitle = 'Job: ' . $title;
        //Si queremos tener los mismos atributos del constructor padre lo hacemos así.
        parent::__construct($newTitle, $description);
    }
    //Polimorfismo.
    //lo que hagamos en este método solo lo va a recibir quién herede de esta clase.
    public function getDurationAsString () {
        $years = floor( $this->months / 12 );
        $extraMonths = $this->months % 12;
    
        return  "Job duration:  $years years and  $extraMonths months. ";
    }
    
}