/*#####crear base de datos*/
create database cursophp

use cursophp;

CREATE TABLE jobs(
id int not null auto_increment primary key,
titl text not null,
description text not null,
visible boolean not null, 
months int not null
);

ALTER TABLE  jobs
add created_at datetime,
add update_at datetime;

SELECT * FROM jobs;