<?php
//Si no hubieramos instalado composer, estas librerias hubieran sido necesarias.
//namespace App\Models;
require_once 'vendor/autoload.php';

//si es necesaria esta linea. y es más corta que las de abajo.
use App\Models\{Job, Project, Printable};


//LLENANDO LOS OBJETOS.
  $job1 = new Job("Delta Developers", "A magnific company"); 
  $job1 -> months = 16;


  $job2 = new Job("Platzi", "A magnific company"); 
  $job2 -> months = 86;

  $project1 = new Project('Project 1', 'Description 1');


  //OBJETOS
  $jobs = [
      $job1,
      $job2
    
  ];

  $projects = [
      $project1
  ];


//Type Hinting: especificar el tipo de dato o valor de una variable, string, int, etc., esto podemos hacerlo con una clase o una interfaz.
  function printElement (Printable $job) {

    //Mostrar solo los trabajos que esten como true y no como false.
    if( $job->visible === false) {
      return;
    }
      echo ' <li class="work-position">';
      echo ' <h5>' . $job->title . '</h5>';
      //esta funcion viene de la interfaz y esta declarada en la clase padre.
      echo ' <p>' . $job->getDescription() . '</p>';
      //funcion dentro de otra funcion
      echo ' <p>' . $job->getDurationAsString() . '</p>';
      echo ' <strong>Achievements:</strong>';
      echo ' <ul>';
      echo ' <li>Lorem ipsum dolor sit amet, 80% consectetuer adipiscing elit.</li>';
      echo ' <li>Lorem ipsum dolor sit amet, 80% consectetuer adipiscing elit.</li>';
      echo ' <li>Lorem ipsum dolor sit amet, 80% consectetuer adipiscing elit.</li>';
      echo ' </ul>';
      echo ' </li>';
  }